package com.rattlehead.witchermodmerger;

import com.rattlehead.auroraio.BIFExtractor;
import com.rattlehead.witchermodmerger.event.StageEvent;
import javafx.application.Application;
import javafx.stage.Stage;
import net.nikr.dds.DDSImageReader;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import java.util.Objects;

import static javafx.application.Application.launch;

public class WitcherModMerger {

    public static void main(String[] args) {
        Objects.requireNonNull(DDSImageReader.class);
        BIFExtractor.loadFileTypeConfiguration("bif-file-types.properties");
        launch(FXApplication.class);
    }

    public static class FXApplication extends Application {

        @Override
        public void start(Stage stage) {
            WeldContainer container = new Weld().initialize();
            container.event().fire(new StageEvent(stage, "fxml/main.fxml", "Witcher Mod Merger"));
        }
    }
}
