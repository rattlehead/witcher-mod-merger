package com.rattlehead.witchermodmerger.controller;

import com.rattlehead.auroraio.BIFExtractor;
import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.data.FileFormat;
import com.rattlehead.witchermodmerger.data.ModFile;
import com.rattlehead.witchermodmerger.event.StageEvent;
import com.rattlehead.witchermodmerger.service.EntityService;
import com.rattlehead.witchermodmerger.service.ModFileService;
import com.rattlehead.witchermodmerger.ui.TooltipTableCell;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.rattlehead.witchermodmerger.util.ExtendedBindings.condition;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.mapping;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.observingElements;
import static java.util.function.Predicate.not;
import static javafx.beans.binding.Bindings.isEmpty;

@Singleton
public class MainController {

    @Inject
    private ModFileService modFileService;
    @Inject
    private EntityService entityService;
    @Inject @Any
    private Event<StageEvent> event;

    @FXML
    private TextField directoryTextField;
    @FXML
    private TableView<Entity> itemsTable;
    @FXML
    private TableColumn<Entity, String> nameColumn;
    @FXML
    private TableColumn<Entity, FileFormat> formatColumn;
    @FXML
    private TableColumn<Entity, String> pathsColumn;
    @FXML
    private Button resetAllButton;
    @FXML
    private Button resolveButton;
    @FXML
    private Button mergeButton;
    @FXML
    private Label loadStatusLabel;

    private final Property<Path> dataDirectory = new SimpleObjectProperty<>();
    private final Property<BIFExtractor> bifExtractor = new SimpleObjectProperty<>();
    private final DirectoryChooser directoryChooser = new DirectoryChooser();

    @Produces @Named("selection")
    public List<Entity> getCurrentSelection() {
        return new ArrayList<>(itemsTable.getSelectionModel().getSelectedItems());
    }

    @Produces @Named("mergeDirectory")
    public Path getMergeDirectory() {
        return getOverrideDirectory().resolve("merged");
    }

    @Produces @Named("bifExtractor")
    public BIFExtractor getGameFileExtractor() {
        return bifExtractor.getValue();
    }

    @FXML
    protected void initialize() {
        directoryTextField.textProperty().bind(mapping(dataDirectory, Path::toString));
        directoryChooser.initialDirectoryProperty().bind(mapping(dataDirectory, path -> path.getParent().toFile()));
        resolveButton.disableProperty()
                .bind(condition(itemsTable.getSelectionModel().getSelectedItems(), not(entityService::canResolve)));
        mergeButton.disableProperty()
                .bind(condition(itemsTable.getSelectionModel().getSelectedItems(), not(entityService::canMerge)));
        itemsTable.getSortOrder().addAll(List.of(formatColumn, pathsColumn, nameColumn));
        itemsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        nameColumn.setCellValueFactory(features -> new ReadOnlyStringWrapper(features.getValue().getName()));
        formatColumn.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(features.getValue().getFormat()));
        pathsColumn.setCellValueFactory(features -> mapping(features.getValue().getAlternatives(), this::joinPaths));
        pathsColumn.setCellFactory(column -> new TooltipTableCell<>());
        dataDirectory.addListener((observable, oldVal, newVal) -> handleChangeDirectory());
    }

    @FXML
    protected void chooseGameDir(ActionEvent event) {
        Window window = ((Node) event.getSource()).getScene().getWindow();
        Optional.ofNullable(directoryChooser.showDialog(window))
                .map(File::toPath)
                .ifPresent(dataDirectory::setValue);
    }

    @FXML
    protected void resolveSelected() {
        event.fire(new StageEvent("fxml/resolve.fxml", "Resolve ambiguous files"));
    }

    @FXML
    protected void mergeSelected() {
        event.fire(new StageEvent("fxml/merge.fxml", "Merge ambiguous files"));
    }

    @FXML
    protected void resetAll() {
        modFileService.clearMerges(getMergeDirectory());
        modFileService.unignoreAll(getOverrideDirectory());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void handleChangeDirectory() {
        ObservableList<ModFile> modFiles = modFileService.observeModFiles(getOverrideDirectory());
        SortedList<Entity> entities = entityService.groupAmbiguousFiles(modFiles).sorted();
        entities.comparatorProperty().bind(itemsTable.comparatorProperty());
        itemsTable.setItems(entities);
        resetAllButton.disableProperty()
                .bind(isEmpty(observingElements(modFiles, ModFile::ignoredProperty).filtered(ModFile::isIgnored)));
        getMergeDirectory().toFile().mkdirs();
        bifExtractor.setValue(null);
        new Thread(this::loadBaseGameFilesIndex).start();
    }

    private String joinPaths(List<ModFile> modFiles) {
        return modFiles.stream()
                .map(ModFile::getInternalPath)
                .sorted()
                .collect(Collectors.joining("; "));
    }

    private Path getOverrideDirectory() {
        return dataDirectory.getValue().resolve("Override");
    }

    private void loadBaseGameFilesIndex() {
        try {
            BIFExtractor extractor = new BIFExtractor(dataDirectory.getValue().resolve("main.key").toFile());
            bifExtractor.setValue(extractor);
            Platform.runLater(() -> loadStatusLabel.setText("Game data load successful!"));
        } catch (IOException e) {
            Platform.runLater(() -> loadStatusLabel.setText("Failed to load game data!"));
            throw new IllegalStateException("Could not read base game data", e);
        }
    }
}
