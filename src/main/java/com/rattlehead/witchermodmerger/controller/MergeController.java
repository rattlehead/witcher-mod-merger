package com.rattlehead.witchermodmerger.controller;

import com.rattlehead.auroraio.BIFExtractor;
import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.service.EntityService;
import com.rattlehead.witchermodmerger.service.ModFileService;
import com.rattlehead.witchermodmerger.strategy.MergeStrategy;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

public class MergeController {

    @Inject
    private ModFileService modFileService;
    @Inject @Named("mergeDirectory")
    private Path mergeDirectory;
    @Inject
    private BIFExtractor bifExtractor;

    @FXML
    private HBox mainPane;

    private final Property<Object> result = new SimpleObjectProperty<>();
    private final MergeStrategy<?> strategy;
    private final Entity entity;

    @Inject
    public MergeController(@Named("selection") List<Entity> selection, EntityService entityService) {
        this.entity = selection.get(0);
        this.strategy = entityService.getMergeStrategy(entity.getFormat()).orElseThrow(IllegalStateException::new);
    }

    @FXML
    protected void initialize() throws IOException {
        Path baseFile = null;
        try {
            if (bifExtractor != null) {
                baseFile = Files.createTempFile(entity.getName(), "base");
                baseFile.toFile().deleteOnExit();
                Files.copy(bifExtractor.getResource(entity.getName()), baseFile, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IllegalArgumentException e) {
            System.err.println("Resource not found in game BIF files: " + entity.getName());
        }
        ObservableValue<?> observable = strategy.renderView(mainPane, entity, baseFile);
        result.bind(observable);
    }

    @FXML
    protected void close(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    protected void process(ActionEvent event) {
        Path target = mergeDirectory.resolve(entity.getName());
        strategy.write(result.getValue(), target);
        modFileService.ignore(entity.getAlternatives().stream()
                .filter(modFile -> !modFile.getPath().equals(target))
                .collect(Collectors.toList()));
        close(event);
    }
}
