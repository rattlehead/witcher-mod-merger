package com.rattlehead.witchermodmerger.controller;

import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.data.ModFile;
import com.rattlehead.witchermodmerger.service.ModFileService;
import com.rattlehead.witchermodmerger.ui.SingleSelectTableCell;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

public class ResolutionController {

    @Inject
    private ModFileService modFileService;
    @Inject @Named("selection")
    private List<Entity> selection;

    @FXML
    protected TableView<Entity> table;

    @FXML
    protected void initialize() {
        table.setItems(FXCollections.observableList(selection));
        table.getSelectionModel().setCellSelectionEnabled(true);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.getColumns().addAll(selection.get(0).getAlternatives()
                .sorted(Comparator.comparing(ModFile::getInternalPath)).stream()
                .map(this::createColumn)
                .collect(Collectors.toList()));
        table.getSelectionModel().selectRange(
                0, table.getColumns().get(0),
                table.getItems().size() - 1, table.getColumns().get(0));
    }

    @FXML
    protected void close(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    protected void process(ActionEvent event) {
        List<ModFile> modFilesToIgnore = table.getSelectionModel().getSelectedCells().stream()
                .flatMap(position -> table.getColumns().stream()
                        .filter(not(position.getTableColumn()::equals))
                        .map(column -> column.getCellData(position.getRow())))
                .map(ModFile.class::cast)
                .collect(Collectors.toList());
        modFileService.ignore(modFilesToIgnore);
        close(event);
    }

    @FXML
    protected void ignoreAll(ActionEvent event) {
        List<ModFile> modFilesToIgnore = selection.stream()
                .map(Entity::getAlternatives)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        modFileService.ignore(modFilesToIgnore);
        close(event);
    }

    private TableColumn<Entity, ModFile> createColumn(ModFile file) {
        TableColumn<Entity, ModFile> column = new TableColumn<>(file.getInternalPath()) {{
            Platform.runLater(() -> setMinWidth(getWidth()));
        }};
        column.setCellValueFactory(features -> new ReadOnlyObjectWrapper<>(
                features.getValue().getAlternatives().stream()
                        .filter(modFile -> modFile.getInternalPath().equals(file.getInternalPath()))
                        .findAny().orElse(null)));
        column.setCellFactory(col -> new SingleSelectTableCell<>());
        return column;
    }
}
