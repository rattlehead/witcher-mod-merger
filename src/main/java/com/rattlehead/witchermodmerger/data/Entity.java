package com.rattlehead.witchermodmerger.data;

import javafx.collections.ObservableList;

public class Entity {

    private final String name;
    private final FileFormat format;
    private final ObservableList<ModFile> alternatives;

    public Entity(String name, FileFormat format, ObservableList<ModFile> alternatives) {
        this.name = name;
        this.format = format;
        this.alternatives = alternatives;
    }

    public String getName() {
        return name;
    }

    public FileFormat getFormat() {
        return format;
    }

    public ObservableList<ModFile> getAlternatives() {
        return alternatives;
    }
}
