package com.rattlehead.witchermodmerger.data;

import java.util.HashMap;
import java.util.Map;

public class FileFormat {

    private static final Map<String, FileFormat> VALUE_MAP = new HashMap<>();

    public static final FileFormat FILE_DDS = valueOf("dds");

    private final String code;

    private FileFormat(String code) {
        this.code = code;
    }

    public static FileFormat valueOf(String code) {
        return VALUE_MAP.computeIfAbsent(code, FileFormat::new);
    }

    public static FileFormat[] values() {
        return VALUE_MAP.values().toArray(FileFormat[]::new);
    }

    @Override
    public String toString() {
        return code;
    }
}
