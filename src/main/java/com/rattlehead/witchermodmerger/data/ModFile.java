package com.rattlehead.witchermodmerger.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.nio.file.Path;

public class ModFile {

    public static final String IGNORED = ".ignored";

    private final String name;
    private final String internalPath;
    private final FileFormat format;
    private final Path directory;
    private final BooleanProperty ignored;

    public ModFile(String name, String internalPath, FileFormat format, Path directory, boolean ignored) {
        this.name = name;
        this.internalPath = internalPath;
        this.format = format;
        this.directory = directory;
        this.ignored = new SimpleBooleanProperty(ignored);
    }

    public String getName() {
        return name;
    }

    public String getInternalPath() {
        return internalPath;
    }

    public FileFormat getFormat() {
        return format;
    }

    public Path getPath() {
        return directory.resolve(isIgnored() ? name + IGNORED : name);
    }

    public boolean isIgnored() {
        return ignored.get();
    }

    public BooleanProperty ignoredProperty() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored.set(ignored);
    }
}
