package com.rattlehead.witchermodmerger.event;

import javafx.stage.Stage;

public class StageEvent {

    private final Stage stage;
    private final String fxml;
    private final String title;

    public StageEvent(Stage stage, String fxml, String title) {
        this.stage = stage;
        this.fxml = fxml;
        this.title = title;
    }

    public StageEvent(String fxml, String title) {
        this(new Stage(), fxml, title);
    }

    public Stage getStage() {
        return stage;
    }

    public String getFxml() {
        return fxml;
    }

    public String getTitle() {
        return title;
    }
}
