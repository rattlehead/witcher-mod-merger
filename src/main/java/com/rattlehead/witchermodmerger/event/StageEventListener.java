package com.rattlehead.witchermodmerger.event;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;

@Singleton
public class StageEventListener {

    @Inject
    private Instance<Object> instance;

    public void showStage(@Observes StageEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(event.getFxml()));
        loader.setControllerFactory(clazz -> instance.select(clazz).get());
        event.getStage().setScene(new Scene(loader.load()));
        event.getStage().setTitle(event.getTitle());
        event.getStage().show();
    }
}
