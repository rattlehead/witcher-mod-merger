package com.rattlehead.witchermodmerger.service;

import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.data.FileFormat;
import com.rattlehead.witchermodmerger.data.ModFile;
import com.rattlehead.witchermodmerger.strategy.MergeStrategy;
import javafx.collections.ObservableList;

import java.util.List;
import java.util.Optional;

public interface EntityService {

    ObservableList<Entity> groupAmbiguousFiles(ObservableList<ModFile> modFiles);

    boolean canResolve(List<Entity> entities);

    boolean canMerge(List<Entity> entities);

    Optional<MergeStrategy<?>> getMergeStrategy(FileFormat format);
}
