package com.rattlehead.witchermodmerger.service;

import com.rattlehead.witchermodmerger.data.ModFile;
import javafx.collections.ObservableList;

import java.nio.file.Path;
import java.util.List;

public interface ModFileService {

    ObservableList<ModFile> observeModFiles(Path overrideDir);

    void ignore(List<ModFile> modFiles);

    void unignore(List<ModFile> modFiles);

    void unignoreAll(Path overrideDirectory);

    void clearMerges(Path mergeDirectory);
}
