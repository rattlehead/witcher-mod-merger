package com.rattlehead.witchermodmerger.service;

import java.io.File;
import java.nio.file.Path;
import java.util.function.Consumer;

public interface ObservationService {

    void observe(Path dir, Consumer<File> onFileCreate, Consumer<File> onFileDelete);

    void stopObservation();
}
