package com.rattlehead.witchermodmerger.service.impl;

import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.data.FileFormat;
import com.rattlehead.witchermodmerger.data.ModFile;
import com.rattlehead.witchermodmerger.service.EntityService;
import com.rattlehead.witchermodmerger.strategy.MergeStrategy;
import com.rattlehead.witchermodmerger.util.SupportedFormats;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.rattlehead.witchermodmerger.util.ExtendedBindings.grouping;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.mappingValues;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.observableValues;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.observingElements;
import static java.util.function.Predicate.not;

@Singleton
public class EntityServiceImpl implements EntityService {

    @Inject
    private Instance<MergeStrategy<?>> instance;

    @Override
    public ObservableList<Entity> groupAmbiguousFiles(ObservableList<ModFile> modFiles) {
        ObservableMap<String, ObservableList<ModFile>> groupedByName = grouping(modFiles, ModFile::getName);
        ObservableMap<String, Entity> mappedToEntity = mappingValues(groupedByName, this::createEntity);
        ObservableList<Entity> allEntities = observableValues(mappedToEntity);
        return observingElements(allEntities, Entity::getAlternatives)
                .filtered(entity -> entity.getAlternatives().size() > 1);
    }

    @Override
    public boolean canResolve(List<Entity> entities) {
        boolean allHaveSameFormat = entities.stream()
                .map(Entity::getFormat)
                .distinct().count() == 1;
        boolean allHaveSameInternalPathSets = entities.stream()
                .map(entity -> entity.getAlternatives().stream()
                        .map(ModFile::getInternalPath)
                        .collect(Collectors.toSet()))
                .distinct().count() == 1;
        return allHaveSameFormat && allHaveSameInternalPathSets;
    }

    @Override
    public boolean canMerge(List<Entity> entities) {
        return entities.size() == 1 && getMergeStrategy(entities.get(0).getFormat()).isPresent();
    }

    @Override
    public Optional<MergeStrategy<?>> getMergeStrategy(FileFormat format) {
        for (MergeStrategy<?> strategy : instance.select()) {
            SupportedFormats supportedFormats = strategy.getClass().getAnnotation(SupportedFormats.class);
            if (supportedFormats != null && Arrays.asList(supportedFormats.value()).contains(format.toString())) {
                return Optional.of(strategy);
            }
        }
        return Optional.empty();
    }

    private Entity createEntity(ObservableList<ModFile> modFiles) {
        String name = modFiles.get(0).getName();
        FileFormat format = modFiles.get(0).getFormat();
        ObservableList<ModFile> alternatives = observingElements(modFiles, ModFile::ignoredProperty)
                .filtered(not(ModFile::isIgnored));
        return new Entity(name, format, alternatives);
    }
}
