package com.rattlehead.witchermodmerger.service.impl;

import com.rattlehead.witchermodmerger.data.FileFormat;
import com.rattlehead.witchermodmerger.data.ModFile;
import com.rattlehead.witchermodmerger.service.ModFileService;
import com.rattlehead.witchermodmerger.service.ObservationService;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.rattlehead.witchermodmerger.util.ExtendedBindings.observableValues;
import static java.util.function.Predicate.not;

@Singleton
public class ModFileServiceImpl implements ModFileService {

    @Inject
    private ObservationService observationService;

    @Override
    public ObservableList<ModFile> observeModFiles(Path overrideDir) {
        ObservableMap<String, ModFile> modFileRegistry = walk(overrideDir)
                .collect(Collectors.toMap(
                        path -> stripIgnore(path.toString()),
                        path -> createModFile(overrideDir, path),
                        (a, b) -> b,
                        FXCollections::observableHashMap));

        observationService.stopObservation();
        observationService.observe(
                overrideDir,
                file -> modFileRegistry.computeIfAbsent(stripIgnore(file.getPath()), k ->
                        createModFile(overrideDir, file.toPath())).setIgnored(isIgnored(file.getPath())),
                file -> Platform.runLater(() -> {
                    if (!modFileRegistry.get(stripIgnore(file.getPath())).getPath().toFile().exists()) {
                        modFileRegistry.remove(stripIgnore(file.getPath()));
                    }
                }));

        return observableValues(modFileRegistry);
    }

    @Override
    public void ignore(List<ModFile> modFiles) {
        modFiles.stream()
                .filter(not(ModFile::isIgnored))
                .map(ModFile::getPath)
                .forEach(path -> move(path, Path.of(path + ModFile.IGNORED)));
    }

    @Override
    public void unignore(List<ModFile> modFiles) {
        modFiles.stream()
                .filter(ModFile::isIgnored)
                .map(ModFile::getPath)
                .forEach(path -> move(path, Path.of(stripIgnore(path.toString()))));
    }

    @Override
    public void unignoreAll(Path overrideDirectory) {
        walk(overrideDirectory)
                .filter(path -> isIgnored(path.toString()))
                .forEach(path -> move(path, Path.of(stripIgnore(path.toString()))));
    }

    @Override
    public void clearMerges(Path mergeDirectory) {
        try {
            FileUtils.cleanDirectory(mergeDirectory.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<Path> walk(Path dir) {
        try {
            return Files.walk(dir)
                    .filter(Files::isRegularFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ModFile createModFile(Path overrideDir, Path path) {
        String internalPath = Objects.toString(overrideDir.relativize(path.getParent()), null);
        String name = stripIgnore(path.getFileName().toString());
        FileFormat format = FileFormat.valueOf(FilenameUtils.getExtension(name).toLowerCase());
        return new ModFile(name, internalPath, format, path.getParent(), isIgnored(path.toString()));
    }

    private void move(Path source, Path target) {
        try {
            Files.move(source, target);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String stripIgnore(String file) {
        return StringUtils.removeEnd(file, ModFile.IGNORED);
    }

    private boolean isIgnored(String file) {
        return file.endsWith(ModFile.IGNORED);
    }
}
