package com.rattlehead.witchermodmerger.service.impl;

import com.rattlehead.witchermodmerger.service.ObservationService;
import javafx.application.Platform;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import javax.inject.Singleton;
import java.io.File;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.function.Consumer;

@Singleton
public class ObservationServiceImpl implements ObservationService {

    private final FileAlterationMonitor fileAlterationMonitor;

    public ObservationServiceImpl() {
        fileAlterationMonitor = new FileAlterationMonitor(50);
        fileAlterationMonitor.setThreadFactory(new BasicThreadFactory.Builder().daemon(true).build());
    }

    @Override
    public void observe(Path dir, Consumer<File> onFileCreate, Consumer<File> onFileDelete) {
        if (fileAlterationMonitor.getObservers().iterator().hasNext()) {
            throw new IllegalStateException("Cannot observe more than one directory");
        }

        FileAlterationObserver observer = new FileAlterationObserver(dir.toFile());
        observer.addListener(new FileAlterationListenerAdaptor() {
            @Override
            public void onFileCreate(File file) {
                Platform.runLater(() -> onFileCreate.accept(file));
            }

            @Override
            public void onFileDelete(File file) {
                Platform.runLater(() -> onFileDelete.accept(file));
            }
        });

        fileAlterationMonitor.addObserver(observer);
        try {
            fileAlterationMonitor.start();
        } catch (Exception e) {
            throw new IllegalStateException("Could not start file alteration observer on directory " + dir, e);
        }
    }

    @Override
    public void stopObservation() {
        Iterator<FileAlterationObserver> iterator = fileAlterationMonitor.getObservers().iterator();
        if (iterator.hasNext()) {
            try {
                fileAlterationMonitor.stop();
            } catch (Exception e) {
                throw new IllegalStateException("Could not stop file alteration observer", e);
            }
            fileAlterationMonitor.removeObserver(iterator.next());
        }
    }
}
