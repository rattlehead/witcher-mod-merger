package com.rattlehead.witchermodmerger.strategy;

import com.rattlehead.witchermodmerger.data.Entity;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

import java.nio.file.Path;

public interface MergeStrategy<T> {

    ObservableValue<T> renderView(Pane mainPane, Entity entity, Path baseFile);

    void write(Object result, Path target);
}
