package com.rattlehead.witchermodmerger.strategy.impl;

import com.rattlehead.auroraio.GFFReader;
import com.rattlehead.auroraio.GFFWriter;
import com.rattlehead.auroraio.data.GFFObject;
import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.strategy.MergeStrategy;
import com.rattlehead.witchermodmerger.ui.GFFDeltaPane;
import com.rattlehead.witchermodmerger.util.SupportedFormats;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.layout.Pane;

import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleConsumer;

@Singleton
@SupportedFormats({"uti", "utc", "utm", "dlg", "spn", "npc"})
public class GFFMergeStrategy implements MergeStrategy<GFFObject> {

    @Override
    public ObservableValue<GFFObject> renderView(Pane mainPane, Entity entity, Path baseFile) {
        GFFObject result;
        if (baseFile != null) {
            result = read(baseFile);
        } else {
            result = read(entity.getAlternatives().get(0).getPath());
        }

        List<GFFDeltaPane> panes = new ArrayList<>();
        panes.add(new GFFDeltaPane("Result", result, true));
        entity.getAlternatives().stream()
                .map(modFile -> new GFFDeltaPane(modFile.getInternalPath(), read(modFile.getPath()), false))
                .forEach(panes::add);

        panes.stream().skip(1).forEach(pane -> pane.bind(panes.get(0)));

        DoubleProperty scrollX = new SimpleDoubleProperty(0.);
        DoubleProperty scrollY = new SimpleDoubleProperty(0.);
        Platform.runLater(() -> panes.forEach(pane -> {
            pane.lookupScrollBar(Orientation.HORIZONTAL).ifPresent(scrollBar ->
                    bindScroll(scrollX, scrollBar.valueProperty(), scrollBar.valueProperty()::setValue));
            pane.lookupScrollBar(Orientation.VERTICAL).ifPresent(scrollBar ->
                    bindScroll(scrollY, scrollBar.valueProperty(), scrollBar.valueProperty()::setValue));
        }));

        mainPane.getChildren().addAll(panes);
        return new ReadOnlyObjectWrapper<>(result);
    }

    @Override
    public void write(Object result, Path target) {
        try (GFFWriter writer = new GFFWriter(target.toFile())) {
            writer.write((GFFObject) result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private GFFObject read(Path file) {
        try (GFFReader reader = new GFFReader(file.toFile())) {
            return reader.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void bindScroll(DoubleProperty scroll, DoubleProperty getter, DoubleConsumer setter) {
        getter.addListener((observable, oldVal, newVal) -> scroll.setValue(newVal));
        scroll.addListener((observable, oldVal, newVal) -> Platform.runLater(() -> setter.accept(scroll.getValue())));
    }
}
