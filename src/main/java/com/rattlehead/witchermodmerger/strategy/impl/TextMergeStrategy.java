package com.rattlehead.witchermodmerger.strategy.impl;

import com.rattlehead.witchermodmerger.data.Entity;
import com.rattlehead.witchermodmerger.strategy.MergeStrategy;
import com.rattlehead.witchermodmerger.ui.TextDeltaPane;
import com.rattlehead.witchermodmerger.util.SupportedFormats;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import org.reactfx.value.Var;

import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleConsumer;

@Singleton
@SupportedFormats({"2da"})
public class TextMergeStrategy implements MergeStrategy<String> {

    @Override
    public ObservableValue<String> renderView(Pane mainPane, Entity entity, Path baseFile) {
        List<TextDeltaPane> panes = new ArrayList<>();

        if (baseFile != null) {
            panes.add(new TextDeltaPane("Result", read(baseFile), true));
        } else {
            panes.add(new TextDeltaPane("Result", read(entity.getAlternatives().get(0).getPath()), true));
        }
        entity.getAlternatives().stream()
                .map(modFile -> new TextDeltaPane(modFile.getInternalPath(), read(modFile.getPath()), false))
                .forEach(panes::add);

        panes.stream().skip(1).forEach(pane -> pane.showDeltasAgainst(panes.get(0)));

        DoubleProperty scrollX = new SimpleDoubleProperty(0.);
        DoubleProperty scrollY = new SimpleDoubleProperty(0.);
        panes.stream()
                .map(TextDeltaPane::getScrollPane)
                .forEach(scrollPane -> {
                    bindScroll(scrollX, scrollPane.estimatedScrollXProperty(), scrollPane::scrollXToPixel);
                    bindScroll(scrollY, scrollPane.estimatedScrollYProperty(), scrollPane::scrollYToPixel);
                });

        mainPane.getChildren().addAll(panes);
        return panes.get(0).textProperty();
    }

    @Override
    public void write(Object result, Path target) {
        try {
            Files.writeString(target, (String) result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String read(Path file) {
        try {
            return Files.readString(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void bindScroll(DoubleProperty scroll, Var<Double> getter, DoubleConsumer setter) {
        getter.addListener((observable, oldVal, newVal) -> scroll.setValue(newVal));
        scroll.addListener((observable, oldVal, newVal) -> Platform.runLater(() -> setter.accept(scroll.getValue())));
    }
}
