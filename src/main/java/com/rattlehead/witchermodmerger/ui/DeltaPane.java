package com.rattlehead.witchermodmerger.ui;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class DeltaPane extends VBox {

    public DeltaPane(String fxmlPath) {
        URL resource = getClass().getClassLoader().getResource(fxmlPath);
        FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(resource));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        HBox.setHgrow(this, Priority.ALWAYS);
    }
}
