package com.rattlehead.witchermodmerger.ui;

import com.rattlehead.auroraio.data.Field;
import com.rattlehead.auroraio.data.GFFObject;
import com.rattlehead.auroraio.data.Struct;
import com.rattlehead.auroraio.util.DataType;
import com.rattlehead.auroraio.util.LocalizedString;
import com.rattlehead.auroraio.util.StructList;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.util.Pair;
import javafx.util.converter.DefaultStringConverter;
import org.joou.UByte;
import org.joou.Unsigned;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.rattlehead.witchermodmerger.util.ExtendedBindings.condition;
import static com.rattlehead.witchermodmerger.util.ExtendedBindings.mapping;
import static java.util.function.Predicate.not;

public class GFFDeltaPane extends DeltaPane {

    private static final String CHANGED_CELL = "changed-cell";
    private static final KeyCodeCombination COPY = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN);
    private static final KeyCodeCombination DELETE = new KeyCodeCombination(KeyCode.DELETE);
    private static final Map<DataType, Function<String, ?>> PARSERS = Map.ofEntries(
            Map.entry(DataType.BYTE, Unsigned::ubyte),
            Map.entry(DataType.CHAR, str -> Optional.of(str)
                    .filter(s -> s.length() == 1)
                    .map(s -> s.charAt(0))
                    .filter(c -> c <= 0xFF)
                    .orElseThrow(IllegalArgumentException::new)),
            Map.entry(DataType.WORD, Unsigned::ushort),
            Map.entry(DataType.SHORT, Short::parseShort),
            Map.entry(DataType.DWORD, Unsigned::uint),
            Map.entry(DataType.INT, Integer::parseInt),
            Map.entry(DataType.DWORD64, Unsigned::ulong),
            Map.entry(DataType.INT64, Long::parseLong),
            Map.entry(DataType.FLOAT, Float::parseFloat),
            Map.entry(DataType.DOUBLE, Double::parseDouble),
            Map.entry(DataType.C_EXO_STRING, Function.identity()),
            Map.entry(DataType.C_RES_REF, str -> Optional.of(str)
                    .filter(s -> s.getBytes().length <= 0xFF)
                    .orElseThrow(IllegalArgumentException::new))
    );

    @FXML
    private Label titleLabel;
    @FXML
    private TreeTableView<Object> treeView;
    @FXML
    private TreeTableColumn<Object, String> structureColumn;
    @FXML
    private TreeTableColumn<Object, String> valueColumn;
    @FXML
    private TreeTableColumn<Object, String> typeColumn;
    @FXML
    private Button mirrorButton;
    @FXML
    private MenuItem addChildMenuItem;
    @FXML
    private MenuItem deleteNodesMenuItem;

    private final List<Runnable> listeners = new ArrayList<>();

    public GFFDeltaPane(String title, GFFObject object, boolean isTarget) {
        super("fxml/fragments/delta-gff-pane.fxml");

        titleLabel.setText(title);
        treeView.setEditable(isTarget);
        treeView.setShowRoot(false);
        mirrorButton.setVisible(!isTarget);
        structureColumn.setCellFactory(this::createCell);
        structureColumn.setCellValueFactory(this::getStructureCellValue);
        valueColumn.setCellFactory(this::createCell);
        valueColumn.setCellValueFactory(this::getValueCellValue);
        typeColumn.setCellFactory(this::createCell);
        typeColumn.setCellValueFactory(this::getTypeCellValue);
        treeView.setOnKeyPressed(event -> {
            if (COPY.match(event) && treeView.isFocused()) {
                handleCopy();
            } else if (isTarget && DELETE.match(event) && treeView.isFocused()) {
                Optional.ofNullable(treeView.getSelectionModel().getSelectedItem())
                        .map(GFFTreeItem.class::cast)
                        .ifPresent(this::handleDeleteNode);
            }
        });
        addChildMenuItem.disableProperty().bind(mapping(treeView.getSelectionModel().getSelectedItems(), list -> {
            if (list.size() != 1) {
                return true;
            } else if (list.get(0).getValue() instanceof Struct) {
                return false;
            } else if (list.get(0).getValue() instanceof Field) {
                return !List.of(DataType.STRUCT, DataType.LIST, DataType.C_EXO_LOC_STRING)
                        .contains(((Field) list.get(0).getValue()).getDataType());
            }
            return true;
        }));
        addChildMenuItem.setOnAction(event ->
                handleAddChild((GFFTreeItem) treeView.getSelectionModel().getSelectedItem()));
        deleteNodesMenuItem.setOnAction(event ->
                Optional.ofNullable(treeView.getSelectionModel().getSelectedItem())
                        .map(GFFTreeItem.class::cast)
                        .ifPresent(this::handleDeleteNode));

        if (!isTarget) {
            treeView.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
        }
        TreeItem<Object> rootItem = new GFFTreeItem(object.getRoot());
        populateTree(rootItem, object.getRoot());
        treeView.setRoot(rootItem);
    }

    public Optional<ScrollBar> lookupScrollBar(Orientation orientation) {
        return treeView.lookupAll(".scroll-bar").stream()
                .filter(ScrollBar.class::isInstance)
                .map(ScrollBar.class::cast)
                .filter(scrollBar -> scrollBar.getOrientation() == orientation)
                .findAny();
    }

    public void bind(GFFDeltaPane to) {
        Runnable listener = () -> {
            compareAndMark((GFFTreeItem) treeView.getRoot(), (GFFTreeItem) to.treeView.getRoot());
            treeView.refresh();
        };
        to.listeners.add(listener);
        listener.run();
        mirrorButton.setOnAction(event -> to.mirrorContent((GFFTreeItem) treeView.getRoot()));
    }

    private void mirrorContent(GFFTreeItem from) {
        Struct source = (Struct) from.getValue();
        Struct current = (Struct) treeView.getRoot().getValue();
        current.setStructId(source.getStructId());
        current.setFields(cloneStruct(source).getFields());
        treeView.getRoot().getChildren().clear();
        populateTree(treeView.getRoot(), current);
        listeners.forEach(Runnable::run);
    }

    private Struct cloneStruct(Struct source) {
        Struct copy = new Struct();
        copy.setStructId(source.getStructId());
        copy.setFields(source.getFields().stream()
                .map(this::cloneField)
                .collect(Collectors.toCollection(ArrayList::new)));
        return copy;
    }

    private Field cloneField(Field source) {
        Field result = new Field(source.getDataType());
        result.setLabel(source.getLabel());

        if (source.getDataType() == DataType.STRUCT) {
            result.setValue(cloneStruct(source.getValue(Struct.class)));
        } else if (source.getDataType() == DataType.LIST) {
            result.setValue(source.getValue(StructList.class).stream()
                    .map(this::cloneStruct)
                    .collect(Collectors.toCollection(StructList::new)));
        } else if (source.getDataType() == DataType.VOID) {
            byte[] original = source.getValue(byte[].class);
            result.setValue(Arrays.copyOf(original, original.length));
        } else if (source.getDataType() == DataType.C_EXO_LOC_STRING) {
            LocalizedString original = source.getValue(LocalizedString.class);
            LocalizedString copy = new LocalizedString();
            copy.setStringRef(original.getStringRef());
            copy.setStrings(new HashMap<>(original.getStrings()));
            result.setValue(copy);
        } else {
            result.setValue(source.getValue(source.getDataType().getValueClass()));
        }
        return result;
    }

    private void handleCopy() {
        TreeItem<Object> item = treeView.getSelectionModel().getSelectedItem();
        if (item != null) {
            ClipboardContent content = new ClipboardContent();
            content.putString(valueColumn.getCellData(item));
            Clipboard.getSystemClipboard().setContent(content);
        }
    }

    private void handleAddChild(GFFTreeItem item) {
        if (item.getValue() instanceof Field &&
                ((Field) item.getValue()).getDataType() == DataType.C_EXO_LOC_STRING) {
            addStringLocalization(item);
        } else if (item.getValue() instanceof Field && ((Field) item.getValue()).getDataType() == DataType.LIST) {
            addStruct(item);
        } else {
            addField(item);
        }
        listeners.forEach(Runnable::run);
    }

    @SuppressWarnings("unchecked")
    private void handleDeleteNode(GFFTreeItem item) {
        if (item.getValue() instanceof Map.Entry) {
            LocalizedString string = ((Field) item.getParent().getValue()).getValue(LocalizedString.class);
            string.getStrings().remove(((Map.Entry<Integer, String>) item.getValue()).getKey());
        } else if (item.getValue() instanceof Struct) {
            List<Struct> structList = ((Field) item.getParent().getValue()).getValue(StructList.class);
            structList.remove((Struct) item.getValue());
        } else if (item.getParent().getValue() instanceof Struct) {
            Struct struct = (Struct) item.getParent().getValue();
            struct.getFields().remove((Field) item.getValue());
        } else {
            Struct struct = ((Field) item.getParent().getValue()).getValue(Struct.class);
            struct.getFields().remove((Field) item.getValue());
        }
        item.getParent().getChildren().remove(item);
        listeners.forEach(Runnable::run);
    }

    @SuppressWarnings("unchecked")
    private void addStringLocalization(GFFTreeItem parent) {
        LocalizedString string = ((Field) parent.getValue()).getValue(LocalizedString.class);
        new MultiInputDialog("Add new localization", List.of(
                new Pair<>("Localization id", new TextField()),
                new Pair<>("Content", new TextField())
        )).showAndWait().map(input -> {
            try {
                Integer id = Integer.parseInt(((TextField) input[0]).getText());
                String content = ((TextField) input[1]).getText();
                return Map.entry(id, content);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }).ifPresent(entry -> {
            parent.getChildren().stream()
                    .map(GFFTreeItem.class::cast)
                    .filter(item -> Objects.equals(((Map.Entry<Integer, String>) item.getValue()).getKey(),
                            entry.getKey()))
                    .findAny()
                    .ifPresent(parent.getChildren()::remove);
            parent.getChildren().add(new GFFTreeItem(entry));

            string.getStrings().remove(entry.getKey());
            string.getStrings().put(entry.getKey(), entry.getValue());
        });
    }

    private void addStruct(GFFTreeItem parent) {
        List<Struct> list = ((Field) parent.getValue()).getValue(StructList.class);
        new MultiInputDialog("Add new struct", List.of(
                new Pair<>("Struct id", new TextField())
        )).showAndWait().map(input -> {
            try {
                Struct struct = new Struct();
                struct.setStructId(Objects.requireNonNull(Unsigned.uint(((TextField) input[0]).getText())));
                struct.setFields(new ArrayList<>());
                return struct;
            } catch (IllegalArgumentException | NullPointerException e) {
                return null;
            }
        }).ifPresent(struct -> {
            parent.getChildren().add(new GFFTreeItem(struct));
            list.add(struct);
        });
    }

    @SuppressWarnings("unchecked")
    private void addField(GFFTreeItem parent) {
        Struct struct;
        if (parent.getValue() instanceof Struct) {
            struct = (Struct) parent.getValue();
        } else {
            struct = ((Field) parent.getValue()).getValue(Struct.class);
        }
        ChoiceBox<DataType> typeInput = new ChoiceBox<>(Arrays.stream(DataType.values())
                .filter(not(DataType.VOID::equals))
                .collect(Collectors.toCollection(FXCollections::observableArrayList)));
        TextField valueOrIdInput = new TextField();
        valueOrIdInput.disableProperty().bind(condition(typeInput.valueProperty(), DataType.LIST::equals));
        new MultiInputDialog("Add new field", List.of(
                new Pair<>("Name", new TextField()),
                new Pair<>("Data type", typeInput),
                new Pair<>("Value or id", valueOrIdInput)
        )).showAndWait().map(input -> {
            String label = ((TextField) input[0]).getText();
            DataType type = ((ChoiceBox<DataType>) input[1]).getValue();
            String valueOrIdRaw = ((TextField) input[2]).getText();
            if (type == null || struct.getFields().stream().map(Field::getLabel).anyMatch(label::equals)) {
                return null;
            }
            try {
                Field field = new Field(type);
                field.setLabel(label);
                if (type == DataType.STRUCT) {
                    Struct value = new Struct();
                    value.setStructId(Objects.requireNonNull(Unsigned.uint(valueOrIdRaw)));
                    value.setFields(new ArrayList<>());
                    field.setValue(value);
                } else if (type == DataType.C_EXO_LOC_STRING) {
                    LocalizedString value = new LocalizedString();
                    value.setStringRef(Objects.requireNonNull(Unsigned.uint(valueOrIdRaw)));
                    value.setStrings(new HashMap<>());
                    field.setValue(value);
                } else if (type == DataType.LIST) {
                    field.setValue(new StructList());
                } else {
                    field.setValue(PARSERS.get(type).apply(Objects.requireNonNull(valueOrIdRaw)));
                }
                return field;
            } catch (IllegalArgumentException | NullPointerException e) {
                return null;
            }
        }).ifPresent(field -> {
            parent.getChildren().add(new GFFTreeItem(field));
            struct.getFields().add(field);
        });
    }

    private void populateTree(TreeItem<Object> parent, Struct struct) {
        for (Field field : struct.getFields()) {
            TreeItem<Object> item = new GFFTreeItem(field);
            parent.getChildren().add(item);
            if (field.getDataType() == DataType.STRUCT) {
                populateTree(item, field.getValue(Struct.class));
            } else if (field.getDataType() == DataType.LIST) {
                List<Struct> structList = field.getValue(StructList.class);
                for (Struct element : structList) {
                    TreeItem<Object> elementItem = new GFFTreeItem(element);
                    item.getChildren().add(elementItem);
                    populateTree(elementItem, element);
                }
            } else if (field.getDataType() == DataType.C_EXO_LOC_STRING) {
                LocalizedString value = field.getValue(LocalizedString.class);
                for (Map.Entry<Integer, String> entry : value.getStrings().entrySet()) {
                    item.getChildren().add(new GFFTreeItem(entry));
                }
            }
        }
    }

    private TreeTableCell<Object, String> createCell(TreeTableColumn<Object, String> column) {
        return new TextFieldTreeTableCell<>(new DefaultStringConverter()) {
            @Override
            public void updateItem(String value, boolean empty) {
                empty = empty || getTreeTableRow().getTreeItem() == null;
                super.updateItem(value, empty);
                setText(empty ? null : value);
                if (empty) {
                    getStyleClass().remove(CHANGED_CELL);
                    setEditable(false);
                } else {
                    GFFTreeItem item = (GFFTreeItem) getTreeTableRow().getTreeItem();
                    if (item.isMarked()) {
                        getStyleClass().add(CHANGED_CELL);
                    } else {
                        getStyleClass().remove(CHANGED_CELL);
                    }
                    setEditable(item.getValue() instanceof Field &&
                            PARSERS.containsKey(((Field) item.getValue()).getDataType()));
                }
            }

            @Override
            public void commitEdit(String value) {
                try {
                    GFFTreeItem item = (GFFTreeItem) getTreeTableRow().getTreeItem();
                    Field field = (Field) item.getValue();
                    Objects.requireNonNull(value);
                    Object data = PARSERS.get(field.getDataType()).apply(value);
                    field.setValue(data);
                } catch (IllegalArgumentException | NullPointerException e) {
                    cancelEdit();
                    return;
                }
                super.commitEdit(value);
                listeners.forEach(Runnable::run);
            }
        };
    }

    private ObservableValue<String> getStructureCellValue(
            TreeTableColumn.CellDataFeatures<Object, String> features) {
        Object item = features.getValue().getValue();
        if (item instanceof Field) {
            return new ReadOnlyStringWrapper(((Field) item).getLabel());
        } else if (item instanceof Struct) {
            return new ReadOnlyStringWrapper("<Struct>");
        } else if (item instanceof Map.Entry) {
            return new ReadOnlyStringWrapper(((Map.Entry<?, ?>) item).getKey().toString());
        }
        return new ReadOnlyStringWrapper();
    }

    private ObservableValue<String> getValueCellValue(TreeTableColumn.CellDataFeatures<Object, String> features) {
        Object item = features.getValue().getValue();
        if (item instanceof Field) {
            Field field = (Field) item;
            if (field.getDataType() == DataType.STRUCT) {
                return new ReadOnlyStringWrapper(field.getValue(Struct.class).getStructId().toString());
            } else if (field.getDataType() == DataType.C_EXO_LOC_STRING) {
                return new ReadOnlyStringWrapper(field.getValue(LocalizedString.class).getStringRef().toString());
            } else if (field.getDataType() != DataType.LIST) {
                return new ReadOnlyStringWrapper(field.getValue(field.getDataType().getValueClass()).toString());
            }
        } else if (item instanceof Map.Entry) {
            return new ReadOnlyStringWrapper(((Map.Entry<?, ?>) item).getValue().toString());
        } else if (item instanceof Struct) {
            return new ReadOnlyStringWrapper(((Struct) item).getStructId().toString());
        }
        return new ReadOnlyStringWrapper();
    }

    private ObservableValue<String> getTypeCellValue(TreeTableColumn.CellDataFeatures<Object, String> features) {
        Object item = features.getValue().getValue();
        if (item instanceof Field) {
            return new ReadOnlyStringWrapper(((Field) item).getDataType().toString());
        }
        return new ReadOnlyStringWrapper();
    }

    @SuppressWarnings("unchecked")
    private void compareAndMark(GFFTreeItem item, GFFTreeItem target) {
        item.expandedProperty().bindBidirectional(target.expandedProperty());

        boolean shouldMark = false;
        List<GFFTreeItem[]> matchedChildren = new ArrayList<>();
        List<GFFTreeItem> unmatchedChildren1 = new ArrayList<>();
        List<GFFTreeItem> unmatchedChildren2 = new ArrayList<>();

        if (item.getValue() instanceof Map.Entry) {
            shouldMark = !Objects.equals(((Map.Entry<Integer, String>) item.getValue()).getValue(),
                    ((Map.Entry<Integer, String>) target.getValue()).getValue());
        } else if (item.getValue() instanceof Struct) {
            matchedChildren = matchChildren(item, target,
                    it -> ((Field) it.getValue()).getLabel(), unmatchedChildren1, unmatchedChildren2);
            shouldMark = !Objects.equals(((Struct) item.getValue()).getStructId(),
                    ((Struct) target.getValue()).getStructId());
        } else {
            Field field1 = ((Field) item.getValue());
            Field field2 = ((Field) target.getValue());
            if (field1.getDataType() != field2.getDataType()) {
                shouldMark = true;
            } else if (field1.getDataType() == DataType.STRUCT) {
                matchedChildren = matchChildren(item, target,
                        it -> ((Field) it.getValue()).getLabel(), unmatchedChildren1, unmatchedChildren2);
                shouldMark = !Objects.equals(field1.getValue(Struct.class).getStructId(),
                        field2.getValue(Struct.class).getStructId());
            } else if (field1.getDataType() == DataType.LIST) {
                matchedChildren = matchChildren(item, target, it ->
                        ((Struct) it.getValue()).getFields().stream()
                                .filter(field -> field.getDataType() == DataType.C_EXO_STRING ||
                                        field.getDataType() == DataType.C_RES_REF)
                                .findFirst()
                                .map(field -> field.getValue(String.class))
                                .or(() -> ((Struct) it.getValue()).getFields().stream()
                                        .filter(field -> field.getDataType() == DataType.BYTE)
                                        .map(field -> field.getValue(UByte.class).toString())
                                        .findFirst()).orElse(""), unmatchedChildren1, unmatchedChildren2);
            } else if (field1.getDataType() == DataType.C_EXO_LOC_STRING) {
                matchedChildren = matchChildren(item, target, it ->
                                ((Map.Entry<Integer, String>) it.getValue()).getKey(),
                        unmatchedChildren1, unmatchedChildren2);
                shouldMark = !Objects.equals(field1.getValue(LocalizedString.class).getStringRef(),
                        field2.getValue(LocalizedString.class).getStringRef());
            } else if (field1.getDataType() == DataType.VOID) {
                shouldMark = !Arrays.equals(field1.getValue(byte[].class), field2.getValue(byte[].class));
            } else {
                shouldMark = !Objects.equals(field1.getValue(field1.getDataType().getValueClass()),
                        field2.getValue(field2.getDataType().getValueClass()));
            }
        }

        matchedChildren.forEach(arr -> compareAndMark(arr[0], arr[1]));
        unmatchedChildren1.stream()
                .flatMap(this::collectRecursive)
                .forEach(it -> ((GFFTreeItem) it).setMarked(true));
        item.setMarked(shouldMark || !unmatchedChildren1.isEmpty() || !unmatchedChildren2.isEmpty() ||
                matchedChildren.stream().map(arr -> arr[0]).anyMatch(GFFTreeItem::isMarked));
    }

    private <T extends Comparable<T>> List<GFFTreeItem[]> matchChildren(GFFTreeItem item1,
                                                                        GFFTreeItem item2,
                                                                        Function<GFFTreeItem, T> keyExtractor,
                                                                        List<GFFTreeItem> unmatched1,
                                                                        List<GFFTreeItem> unmatched2) {
        Iterator<GFFTreeItem> iterator1 = item1.getChildren().stream()
                .map(GFFTreeItem.class::cast)
                .sorted(Comparator.comparing(keyExtractor))
                .collect(Collectors.toList()).iterator();
        Iterator<GFFTreeItem> iterator2 = item2.getChildren().stream()
                .map(GFFTreeItem.class::cast)
                .sorted(Comparator.comparing(keyExtractor))
                .collect(Collectors.toList()).iterator();

        List<GFFTreeItem[]> result = new ArrayList<>();
        while (iterator1.hasNext() && iterator2.hasNext()) {
            GFFTreeItem child1 = iterator1.next();
            T key1 = keyExtractor.apply(child1);
            GFFTreeItem child2 = iterator2.next();
            T key2 = keyExtractor.apply(child2);

            while (key1.compareTo(key2) != 0) {
                if (key1.compareTo(key2) < 0) {
                    unmatched1.add(item1);
                    if (!iterator1.hasNext()) {
                        break;
                    }
                    child1 = iterator1.next();
                    key1 = keyExtractor.apply(child1);
                }
                if (key1.compareTo(key2) > 0) {
                    unmatched2.add(item2);
                    if (!iterator2.hasNext()) {
                        break;
                    }
                    child2 = iterator2.next();
                    key2 = keyExtractor.apply(child2);
                }
            }
            if (key1.equals(key2)) {
                result.add(new GFFTreeItem[]{child1, child2});
            }
        }
        while (iterator1.hasNext()) {
            unmatched1.add(iterator1.next());
        }
        if (iterator2.hasNext()) {
            unmatched2.add(iterator2.next());
        }
        return result;
    }

    private Stream<TreeItem<?>> collectRecursive(TreeItem<?> item) {
        return Stream.concat(Stream.of(item), item.getChildren().stream().flatMap(this::collectRecursive));
    }

    private static class GFFTreeItem extends TreeItem<Object> {

        private boolean marked = false;

        public GFFTreeItem(Object item) {
            super(item);
        }

        public boolean isMarked() {
            return this.marked;
        }

        public void setMarked(boolean marked) {
            this.marked = marked;
        }
    }
}
