package com.rattlehead.witchermodmerger.ui;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class MultiInputDialog extends Dialog<Node[]> {

    private final List<Node> inputs = new ArrayList<>();

    public MultiInputDialog(String title, List<Pair<String, Node>> elements) {
        setTitle(title);
        VBox layout = new VBox(5);
        for (Pair<String, Node> pair : elements) {
            HBox field = new HBox(5);
            field.setAlignment(Pos.TOP_RIGHT);

            Label label = new Label(pair.getKey() + ":");
            label.prefHeightProperty().bind(field.heightProperty());

            inputs.add(pair.getValue());
            field.getChildren().addAll(label, pair.getValue());
            layout.getChildren().add(field);
        }
        getDialogPane().setContent(layout);
        getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
        setResultConverter(buttonType -> ButtonType.OK.equals(buttonType) ? getInputValues() : null);
        Platform.runLater(() -> getDialogPane().getScene().getWindow().sizeToScene());
    }

    private Node[] getInputValues() {
        return inputs.toArray(Node[]::new);
    }
}
