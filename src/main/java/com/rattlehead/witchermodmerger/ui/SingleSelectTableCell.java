package com.rattlehead.witchermodmerger.ui;

import com.rattlehead.witchermodmerger.data.FileFormat;
import com.rattlehead.witchermodmerger.data.ModFile;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

public class SingleSelectTableCell<S> extends TableCell<S, ModFile> {

    private static final double IMAGE_FIT_WIDTH = 200;
    private static final double IMAGE_FIT_HEIGHT = 200;

    private final ImageView imageView;

    public SingleSelectTableCell() {
        imageView = new ImageView();
        imageView.setScaleY(-1);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(IMAGE_FIT_WIDTH);
        imageView.setFitHeight(IMAGE_FIT_HEIGHT);

        addEventHandler(MouseEvent.MOUSE_PRESSED, this::handleMouse);
        addEventHandler(MouseEvent.MOUSE_RELEASED, this::handleMouse);
    }

    @Override
    protected void updateItem(ModFile modFile, boolean empty) {
        super.updateItem(modFile, empty);
        setText(empty ? null : modFile.getName());

        if (!empty && modFile.getFormat() == FileFormat.FILE_DDS) {
            imageView.setImage(loadImage(modFile));
            setGraphic(imageView);
        } else {
            imageView.setImage(null);
            setGraphic(null);
        }
    }

    @Override
    public void updateSelected(boolean selected) {
        if (selected) {
            Platform.runLater(() -> getOtherColumns().forEach(this::clearSelection));
        } else if (!isAnyOtherColumnSelected()) {
            return;
        }
        super.updateSelected(selected);
    }

    private Image loadImage(ModFile modFile) {
        ImageReader imageReader = ImageIO.getImageReadersBySuffix(modFile.getFormat().toString()).next();
        File resource = modFile.getPath().toFile();
        try (ImageInputStream inputStream = new FileImageInputStream(resource)) {
            imageReader.setInput(inputStream);
            return SwingFXUtils.toFXImage(imageReader.read(0), null);
        } catch (IOException e) {
            return null;
        }
    }

    private void handleMouse(MouseEvent event) {
        if (event.isShiftDown() && event.isControlDown()) {
            Event newEvent = cloneEven(event, true, false);
            Event.fireEvent(event.getTarget(), newEvent);
            event.consume();
        } else if (!event.isShiftDown() && !event.isControlDown()) {
            Event newEvent = cloneEven(event, false, true);
            Event.fireEvent(event.getTarget(), newEvent);
            event.consume();
        }
    }

    private MouseEvent cloneEven(MouseEvent event, boolean shift, boolean control) {
        return new MouseEvent(event.getSource(), event.getTarget(), event.getEventType(), event.getX(),
                event.getY(), event.getScreenX(), event.getScreenY(), event.getButton(), event.getClickCount(),
                shift, control, event.isAltDown(), event.isMetaDown(), event.isPrimaryButtonDown(),
                event.isMiddleButtonDown(), event.isSecondaryButtonDown(), event.isSynthesized(),
                event.isPopupTrigger(), event.isStillSincePress(), event.getPickResult());
    }

    private List<TableColumn<S, ?>> getOtherColumns() {
        return getTableColumn().getTableView().getColumns().stream()
                .filter(not(getTableColumn()::equals))
                .collect(Collectors.toList());
    }

    private boolean isAnyOtherColumnSelected() {
        return getOtherColumns().stream()
                .anyMatch(col -> getTableView().getSelectionModel().isSelected(getIndex(), col));
    }

    private void clearSelection(TableColumn<S, ?> column) {
        getTableView().getSelectionModel().clearSelection(getIndex(), column);
    }
}
