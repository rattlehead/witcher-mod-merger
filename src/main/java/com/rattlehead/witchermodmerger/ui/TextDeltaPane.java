package com.rattlehead.witchermodmerger.ui;

import com.github.difflib.DiffUtils;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.DeltaType;
import com.rattlehead.witchermodmerger.strategy.impl.TextMergeStrategy;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.StyleClassedTextArea;
import org.fxmisc.richtext.model.Paragraph;

import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.function.Predicate.not;

public class TextDeltaPane extends DeltaPane {

    private static final String CHANGED_SEGMENT = "changed-segment";
    private static final String CHANGED_LINE = "changed-line";
    private static final String INSERTED_LINE = "inserted-line";

    @FXML
    private Label titleLabel;
    @FXML
    private VirtualizedScrollPane<?> scrollPane;
    @FXML
    private StyleClassedTextArea textArea;

    public TextDeltaPane(String title, String content, boolean isTarget) {
        super("fxml/fragments/delta-text-pane.fxml");

        titleLabel.setText(title);
        textArea.appendText(content);
        textArea.getUndoManager().forgetHistory();
        textArea.moveTo(0);
        textArea.requestFollowCaret();
        textArea.setEditable(isTarget);

        if (isTarget) {
            textArea.plainTextChanges().subscribe(change -> {
                if (change.getPosition() != change.getInsertionEnd()) {
                    textArea.clearStyle(change.getPosition(), change.getInsertionEnd());
                }
            });
        }
    }

    public VirtualizedScrollPane<?> getScrollPane() {
        return scrollPane;
    }

    public ObservableValue<String> textProperty() {
        return textArea.textProperty();
    }

    public void showDeltasAgainst(TextDeltaPane pane) {
        pane.getTextArea().plainTextChanges().subscribe(change -> computeDiffAndUpdateStyles(pane.getTextArea()));
        computeDiffAndUpdateStyles(pane.getTextArea());
    }

    private void computeDiffAndUpdateStyles(StyleClassedTextArea modelArea) {
        List<String> toCompare = textArea.getParagraphs().map(Paragraph::getText);
        List<String> toCompareAgainst = modelArea.getParagraphs().map(Paragraph::getText);

        for (AbstractDelta<String> delta : DiffUtils.diff(toCompareAgainst, toCompare, true).getDeltas()) {
            if (delta.getType() == DeltaType.EQUAL) {
                processEqualDelta(delta);
            } else if (delta.getType() == DeltaType.INSERT) {
                processInsertDelta(delta);
            } else if (delta.getType() == DeltaType.CHANGE) {
                processChangeDelta(delta);
            }
        }
    }

    private void processEqualDelta(AbstractDelta<String> delta) {
        int offset = delta.getTarget().getPosition();

        IntStream.range(0, delta.getTarget().getLines().size())
                .filter(line -> textArea.getStyleSpans(line + offset).styleStream()
                        .anyMatch(not(Collection::isEmpty)))
                .forEach(line -> textArea.clearStyle(line + offset));
    }

    private void processInsertDelta(AbstractDelta<String> delta) {
        int offset = delta.getTarget().getPosition();

        IntStream.range(0, delta.getTarget().getLines().size())
                .forEach(line -> textArea.setStyle(line + offset, List.of(INSERTED_LINE)));
    }

    private void processChangeDelta(AbstractDelta<String> delta) {
        int offset = delta.getTarget().getPosition();

        IntStream.range(0, delta.getTarget().getLines().size())
                .forEach(line -> textArea.setStyle(line + offset, List.of(CHANGED_LINE)));

        if (delta.getSource().getLines().size() == delta.getTarget().getLines().size()) {
            IntStream.range(0, delta.getTarget().getLines().size())
                    .forEach(line -> {
                        String sourceLine = delta.getSource().getLines().get(line);
                        String targetLine = delta.getTarget().getLines().get(line);
                        processInlineDeltas(line + offset,
                                DiffUtils.diffInline(sourceLine, targetLine).getDeltas());
                    });
        }
    }

    private void processInlineDeltas(int paragraph, List<AbstractDelta<String>> inlineDeltas) {
        for (AbstractDelta<String> delta : inlineDeltas) {
            int startPosition = delta.getTarget().getPosition();
            int endPosition = startPosition + delta.getTarget().getLines().stream().mapToInt(String::length).sum();

            // inline delete deltas - highlight one character before or after it if possible
            if (startPosition == endPosition && startPosition > 0) {
                startPosition--;
            } else if (startPosition == endPosition && endPosition < textArea.getParagraphLength(paragraph) - 1) {
                endPosition++;
            }
            // if not - then it's *non-empty line* <-> *empty line* delta which is pretty noticeable on its own
            textArea.setStyle(paragraph, startPosition, endPosition, List.of(CHANGED_SEGMENT));
        }
    }

    private StyleClassedTextArea getTextArea() {
        return textArea;
    }
}
