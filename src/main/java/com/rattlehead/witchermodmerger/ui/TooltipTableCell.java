package com.rattlehead.witchermodmerger.ui;

import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;

public class TooltipTableCell<S> extends TableCell<S, String> {

    private final Tooltip tooltip = new Tooltip();

    @Override
    protected void updateItem(String value, boolean empty) {
        super.updateItem(value, empty);
        setText(empty ? null : value);
        setTooltip(empty ? null : tooltip);
        tooltip.setText(empty ? null : value);
    }
}
