package com.rattlehead.witchermodmerger.util;

import javafx.beans.Observable;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class ExtendedBindings {

    private ExtendedBindings() {
    }

    public static <P, R> Binding<R> mapping(ObservableValue<P> observable,
                                            Function<? super P, ? extends R> mapper,
                                            R nullDefault) {
        return Bindings.createObjectBinding(() ->
                Optional.ofNullable(observable.getValue()).<R>map(mapper).orElse(nullDefault), observable);
    }

    public static <P, R> Binding<R> mapping(ObservableValue<P> observable,
                                            Function<? super P, ? extends R> mapper) {
        return mapping(observable, mapper, null);
    }

    public static <P> Binding<Boolean> condition(ObservableValue<P> observable,
                                                 Predicate<? super P> predicate) {
        return mapping(observable, predicate::test);
    }

    public static <P, R> Binding<R> mapping(ObservableList<P> observableList,
                                            Function<ObservableList<P>, ? extends R> mapper) {
        return Bindings.createObjectBinding(() -> mapper.apply(observableList), observableList);
    }

    public static <P> Binding<Boolean> condition(ObservableList<P> observableList,
                                                 Predicate<ObservableList<P>> predicate) {
        return mapping(observableList, predicate::test);
    }

    @SafeVarargs
    public static <R> ObservableList<R> observingElements(ObservableList<R> source,
                                                          Function<? super R, Observable>... observableProviders) {
        ObservableList<R> result = FXCollections.observableArrayList(item ->
                Arrays.stream(observableProviders)
                        .map(func -> func.apply(item))
                        .toArray(Observable[]::new));
        Bindings.bindContent(result, source);
        return result;
    }

    public static <K, V> ObservableList<V> observableValues(ObservableMap<K, V> observableMap) {
        ObservableList<V> result = FXCollections.observableArrayList(observableMap.values());

        observableMap.addListener((MapChangeListener<K, V>) change -> {
            if (change.wasAdded()) {
                result.add(change.getValueAdded());
            } else if (change.wasRemoved()) {
                result.remove(change.getValueRemoved());
            }
        });
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> ObservableMap<K, ObservableList<V>> grouping(ObservableList<V> observableList,
                                                                      Function<? super V, ? extends K> groupFunction) {
        ObservableMap<K, ObservableList<V>> result = FXCollections.observableHashMap();
        result.putAll(observableList.stream()
                .collect(Collectors.groupingBy(groupFunction,
                        Collectors.toCollection(FXCollections::observableArrayList))));

        observableList.addListener((ListChangeListener<V>) change -> {
            while(change.next()) {
                if (change.wasAdded()) {
                    change.getAddedSubList().forEach(addedItem -> {
                        K key = groupFunction.apply(addedItem);
                        if (result.containsKey(key)) {
                            result.get(key).add(addedItem);
                        } else {
                            result.put(key, FXCollections.observableArrayList(addedItem));
                        }
                    });
                }
                if (change.wasRemoved()) {
                    change.getRemoved().forEach(removedItem -> {
                        K key = groupFunction.apply(removedItem);
                        result.get(key).remove(removedItem);
                        if (result.get(key).isEmpty()) {
                            result.remove(key);
                        }
                    });
                }
            }
        });
        return result;
    }

    public static <K, P, R> ObservableMap<K, R> mappingValues(ObservableMap<K, P> observableMap,
                                                              Function<? super P, ? extends R> mapper) {
        ObservableMap<K, R> result = FXCollections.observableHashMap();
        observableMap.forEach((key, value) -> result.put(key, mapper.apply(value)));

        observableMap.addListener((MapChangeListener<K, P>) change -> {
            if (change.wasAdded()) {
                result.put(change.getKey(), mapper.apply(change.getValueAdded()));
            } else if (change.wasRemoved()) {
                result.remove(change.getKey());
            }
        });
        return result;
    }
}
